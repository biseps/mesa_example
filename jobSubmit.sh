#!/bin/bash


# Which inlists to search for?

# inlist_folder='./inlists_pre_ms'
inlist_folder='./inlists_ms'

# search_pattern='inlist_zams_to_giant_*'
search_pattern='inlist_zams_to_h1_exhaust_*'


# cluster variables
JOB_NAME='mesa_h1_exhaust'
CLUSTER_MSGS='./cluster_msgs'


# submit job to cluster
job_ids=$(qsub -terse                                 \
          -v inlist_folder="$inlist_folder"           \
          -v search_pattern="$search_pattern"         \
          -N "$JOB_NAME"                              \
          -o "$CLUSTER_MSGS"                          \
          run_cluster.sh)


#!/bin/bash


# Validate command-line arguments
if [ "$#" -ne 1 ];
then
    echo "Missing parameter! "
    echo "Please supply the name of an inlist template file (e.g. inlist_zams_to_giant)"
    exit 1
fi

# inlist_template="./inlist_zams_to_giant"
inlist_template="$1"

# ensure template exits
if [ ! -f "$inlist_template" ]; then
    printf "\nError!: Inlist template '$inlist_template' does not exist.\n"
    exit 1
fi


# a new "inlist" file will be created
# for each mass in this array
initial_mass_array=("1.0" "1.1" "1.2" "1.3" "1.4" "1.5" "1.6" "1.7" "1.8" "1.9" "2.0")
# initial_mass_array=("1.0" "1.1" )


# Tell user which template we're using
printf "\n Using template file: $inlist_template \n\n"


# search inlist template for MESA variables to be replaced
search_string1='initial_mass = xxx'
search_string2='xxx_msun'
search_string3='initial_z = xxxxxx'

# Use this metallicity
initial_z_value="0.02d0"



# loop through the mass array,
# creating a new inlist file for each mass,
# and update the inlist variables appropriately
for mass in "${initial_mass_array[@]}"
do

    printf "\nMass: $mass\n"

    # create new inlist filename
    new_inlist="$inlist_template""_$mass"

    # make a copy of the template
    cp  $inlist_template  $new_inlist

    # tell user
    printf "created $new_inlist ... \n"


    # update the MESA variables
    replace_string1="initial_mass = $mass"
    replace_string2="$mass""_msun"
    replace_string3="initial_z = $initial_z_value"


    # update the inlist variables
    sed -i  "s/$search_string1/$replace_string1/g"   "$new_inlist" 
    sed -i  "s/$search_string2/$replace_string2/g"   "$new_inlist" 
    sed -i  "s/$search_string3/$replace_string3/g"   "$new_inlist" 

done

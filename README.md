# MESA example  #

This repo contains scripts and fortran "inlists" to evolve stellar evolution tracks. In this example 11 different tracks are evolved, each track with an different intial mass ranging from 1.0 Msun to 2.0 Msun

### compile MESA

Before creating any evolution tracks, the fortran code must be compiled.

    source ./env_mesa.sh
    ./mk

If everything compiles ok, a new file called "star_solar_life" should appear. The "env_mesa.sh" creates several UNIX environment variables which are need to make the compilation work.

### Create "inlists"

"Inlists" are parameter files that tell MESA what type of star to create. For this example, a separate "inlist" file must be created for each stellar mass.

In the following code example, a new main sequence (ms) inlist is created for each initial mass

    cd inlists_ms
    ./create_inlists inlist_zams_to_h1_exhaust

10 new inlist files will be created

### Run MESA on the cluster.

The "jobSubmit.sh" script will call "run_cluster.sh" which launches a cluster job which executes the stellar evolution code.

Output files will appear in the "logs_ms" folder and also in the "models_ms" folder.

    ./jobSubmit.sh
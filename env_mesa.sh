#!/bin/bash

# Setup Mesa Star Environment


if [[ "${BASH_SOURCE[0]}" = "${0}" ]]
then
    echo "You must SOURCE this script!"
    echo "(Otherwise the MESA environment variables will not be set properly)"
    exit
fi

export MESA_DIR="/padata/beta/users/efarrell/repos/mesa-6208"
#export MESA_DIR="$HOME/mesa"

export MESASDK_ROOT="/padata/beta/users/efarrell/repos/mesasdk"
#export MESASDK_ROOT="$HOME/mesasdk"

export OMP_NUM_THREADS=8
#export OMP_NUM_THREADS=2

[[ -s $MESASDK_ROOT/bin/mesasdk_init.sh ]] && . $MESASDK_ROOT/bin/mesasdk_init.sh


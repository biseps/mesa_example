#!/bin/bash

#$ -l s_rt=100:00:00,virtual_free=6G,h_vmem=6G
#$ -M enda.farrell@open.ac.uk
#$ -m as
#$ -P science
#$ -S /bin/bash
#$ -j y
#$ -cwd



printf "\n inlist folder: $inlist_folder\n"
printf "\n search pattern: $search_pattern\n"


# setup mesa environment
. ./env_mesa.sh


# show our MESA environment
printf "\n\n MESA variables:\n"
env | grep -i "mesa"
env | grep -i "OMP_NUM_THREADS"


find $inlist_folder -iname "$search_pattern" -print | while read current_inlist
do
    printf "\n\ncurrent_inlist: $current_inlist\n"

    # make the current inlist active
    cp "$current_inlist" './active_inlist'

    # run MESA - it will pick up parameters from file 'active_inlist'
    ./rn

done


#!/bin/bash

output_movie='1.5_hr.mp4'

ffmpeg_64 -r 4 -pattern_type glob -i './png/1\.5*.png' -vb 20M -vf "scale=640:trunc(ow/a/2)*2" -c:v libx264 $output_movie



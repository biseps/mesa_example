
&star_job

      show_log_description_at_start = .false.
      
      !show_net_species_info = .true.
      !show_net_reactions_info = .true.

      create_pre_main_sequence_model = .true.

      save_model_when_terminate = .true.
      save_model_filename = 'final.mod'
      
      write_profile_when_terminate = .true.
      filename_for_profile_when_terminate = 'final_profile.data'

      kappa_file_prefix = 'gs98'
      
      change_lnPgas_flag = .true.
      new_lnPgas_flag = .true.

      change_initial_net = .true.      
      new_net_name = 'o18_and_ne22.net'

      pre_ms_relax_num_steps = 50
      
      new_surface_rotation_v = 2 ! solar (km sec^1)
      set_near_zams_surface_rotation_v_steps = 10 ! to turn on rotation when near zams
         ! if rotation_flag is false and L_nuc_burn_total >= L_phot and this control > 0
         ! then turn on rotation and set set_surf_rotation_v_step_limit to
         ! the current model_number plus this control

      ! pgstar_flag = .true.
      pgstar_flag = .false.


/ ! end of star_job namelist

&controls

      ! check for retries and backups as part of test_suite
      ! you can/should delete this for use outside of test_suite
         max_number_backups = 200
         max_number_retries = 500
         
         
         max_model_number = 20000

      initial_mass = 1.0
      initial_z = 0.02d0

      ! ! when running plotting window reduce the disk i/o
      ! photostep = 100
      ! profile_interval = 200
      ! history_interval = 50
      ! terminal_cnt = 50

      ! when not running plotting window
      photostep = 100
      profile_interval = 50
      history_interval = 10
      terminal_cnt = 10

      ! Just put a basic stop

      ! time steps

      ! Stop when reach current solar age
      max_age = 4.61d9 ! 4.57d9 + 4d7 for pre-ms
      
      
      use_Type2_opacities = .true.
      Zbase = 0.02d0

      D_DSI_factor = 0
      
      varcontrol_target = 1d-3
      mesh_delta_coeff = 1.5


      write_header_frequency = 10


      smooth_convective_bdy = .true.                  
      convective_bdy_weight = 1
      
      RGB_wind_scheme = 'Reimers'
      AGB_wind_scheme = 'Blocker'
      RGB_to_AGB_wind_switch = 1d-4
      Reimers_wind_eta = 0.7d0  
      Blocker_wind_eta = 0.7d0  
      
      ! FOR DEBUGGING

      !report_hydro_solver_progress = .true. ! set true to see info about newton iterations
      !report_ierr = .true. ! if true, produce terminal output when have some internal error
      !hydro_show_correction_info = .true.
      
      !max_years_for_timestep = 3.67628942044319d-05

      !report_why_dt_limits = .true.
      !report_all_dt_limits = .true.
      
      !show_mesh_changes = .true.
      !mesh_dump_call_number = 5189
      !okay_to_remesh = .false.
      
      !trace_evolve = .true.
            

      ! hydro debugging
      !hydro_check_everything = .true.
      !hydro_inspectB_flag = .true.
      
      !hydro_numerical_jacobian = .true.
      !hydro_save_numjac_plot_data = .true.
      !small_mtx_decsol = 'lapack'
      !large_mtx_decsol = 'lapack'
      !hydro_dump_call_number = 195

      !trace_newton_bcyclic_solve_input = .true. ! input is "B" j k iter B(j,k)
      !trace_newton_bcyclic_solve_output = .true. ! output is "X" j k iter X(j,k)
      
      !trace_newton_bcyclic_steplo = 1 ! 1st model number to trace
      !trace_newton_bcyclic_stephi = 1 ! last model number to trace
      
      !trace_newton_bcyclic_iterlo = 2 ! 1st newton iter to trace
      !trace_newton_bcyclic_iterhi = 2 ! last newton iter to trace
      
      !trace_newton_bcyclic_nzlo = 1 ! 1st cell to trace
      !trace_newton_bcyclic_nzhi = 10000 ! last cell to trace; if < 0, then use nz as nzhi
      
      !trace_newton_bcyclic_jlo = 1 ! 1st var to trace
      !trace_newton_bcyclic_jhi = 100 ! last var to trace; if < 0, then use nvar as jhi
      
      !trace_k = 0
      

/ ! end of controls namelist



&pgstar

         ! Grid4_win_flag = .True.
         Grid4_win_flag = .false.
         Grid4_win_width = 10

        ! aspect_ratio = height/width
         Grid4_win_aspect_ratio = 1 
         !Grid4_win_aspect_ratio = 0.75 


         
         ! ----------------------------------------------
         ! Image file (png, jpg) output
         ! ----------------------------------------------

         ! Grid4_file_flag = .true.
         Grid4_file_flag = .false.

        ! output when mod(model_number,Grid4_file_cnt)==0
         ! Grid4_file_cnt = 10            
         Grid4_file_cnt = 100            

        ! negative means use same value as for window
         Grid4_file_width = 25          
         !Grid4_file_width = -1          


         ! hmmm cant get jpeg to work for pgplot
         !file_device = 'jpg'
         !file_extension = 'jpg' 

         Grid4_file_dir = 'png'
         Grid4_file_prefix = 'grid4_'


        ! negative means use same value as for window
         Grid4_file_aspect_ratio = -1   
         
         
         

         
      ! Grid6_win_flag = .true.
      Grid6_win_flag = .false.
      Grid6_win_width = 11
         
      !Grid6_file_flag = .true.
      Grid6_file_dir = 'png'
      Grid6_file_prefix = 'grid6_'

      ! Grid6_file_cnt = 5 ! output when mod(model_number,Grid6_file_cnt)==0
      Grid6_file_cnt = 10 ! output when mod(model_number,Grid6_file_cnt)==0

      Grid6_file_width = -1 ! (inches) negative means use same value as for window
      Grid6_file_aspect_ratio = -1 ! negative means use same value as for window

      Summary_Burn_xaxis_name = 'mass' 
      Summary_Burn_xaxis_reversed = .false.
      Summary_Burn_xmin = 0.00 ! -101d0 ! only used if /= -101d0
      Summary_Burn_xmax = 2.1  ! only used if /= -101d0
      
      Abundance_xaxis_name = 'mass' 
      Abundance_xaxis_reversed = .false.
      ! power xaxis limits -- to override system default selections
      Abundance_xmin = 0.00 ! -101d0 ! only used if /= -101d0
      Abundance_xmax = 2.1 ! only used if /= -101d0
      Abundance_log_mass_frac_min = -6 ! only used if < 0

      !Profile_Panels4_win_flag = .true.
      !Profile_Panels4_win_width = 6
         
      ! Abundance window -- current model abundance profiles
      
         ! Abundance_win_flag = .true.
         Abundance_win_flag = .false.
      
         Abundance_win_width = 9
         Abundance_win_aspect_ratio = 0.75 ! aspect_ratio = height/width
   
/ ! end of pgstar namelist

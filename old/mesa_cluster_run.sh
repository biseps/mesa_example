#!/bin/bash


#$ -N mesa_run

# The SGE batch system uses the current directory as working directory.
# Both files (output.dat and error.dat) will be placed in the current
# directory. The batch system assumes to find the executable in this directory.
#$ -cwd

# Redirect output stream 
#$ -o cluster_output.dat

# Redirect error stream 
#$ -e cluster_error.dat

# Send status information to this email address.
#$ -M enda.farrell@open.ac.uk

# Send an e-mail when the job is aborted or suspended
#$ -m as

# OU IMPACT cluster uses projects
#$ -P science

# Specify interpreting shell for the job
#$ -S /bin/bash


# setup mesa environment
. ./env_mesa.sh


## echo out our MESA environment
echo
echo MESA variables:
echo
env | grep -i "mesa"


# Execute script to create pre-main sequence models
# ./run_all_pre_ms.sh


# Execute script to create zams to giant branch models
./run_all_ms.sh

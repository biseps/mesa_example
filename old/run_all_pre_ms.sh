#!/bin/bash


inlist_folder="./inlists_pre_ms"
search_pattern="inlist_pre_ms_to_zams_*"



find $inlist_folder -iname "$search_pattern" -print | while read current_inlist
do
    printf "current_inlist: $current_inlist\n"

    # make the current inlist active
    cp "$current_inlist" './active_inlist'

    # run MESA - it will pick up the active inlist
    ./rn
done

